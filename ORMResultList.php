<?php

namespace Flowlu;

class ORMResultList implements \Iterator
{
	private $index = 0;
	private $page = 0;
	private $total = 0;
	private $items = [];
	private $result_id = NULL;
	private $model = NULL;
	private $modelClass = NULL;

	public function __construct($model, $filters = [], $foreign_key = NULL){
		$this->index = 0;
		$this->items = [];
		$this->model = $model;

		$normalizeFilters = function($input) use (&$normalizeFilters){
			return array_map(function($entry) use (&$normalizeFilters) {
				if(is_array($entry)){
					return $normalizeFilters($entry);
				}

				if($entry instanceof \Flowlu\ORM){
					if(isset($entry->object()['id'])){
						return $entry->object()['id'];
					}else{
						return NULL;
					}
				}

				return $entry;
			}, $input);
		};

		$filters = $normalizeFilters($filters);

		if(!$foreign_key){
			$_limit = isset($filters['limit'])? $filters['limit']: 0;

			$_fields = NULL;
			if(isset($filters['fields'])){
				$_fields = array_merge($filters['fields'], ['id']);
				unset($filters['fields']);
			}
			$_lazy_load = is_array($_fields);

			$this->modelClass = \Flowlu\Model::_findModelClass([
				'module' => $model->target('module'),
				'model' => $model->target('model')
			]);

			$response = $model->execute(\Flowlu\ORM::METHOD_LIST, array_merge(['limit' => $_limit], ['filter' => $filters, 'fields' => $_fields]));

			if(isset($response['_result_id'])){
				$this->result_id = $response['_result_id'];
				$this->page = 0;
			}elseif(isset($response['items'])){
				$page = intval($response['page']);
				$limit = intval($response['count']);
				$start_position = ($page - 1) * $limit;
				$end_position = $page * $limit - 1;
				
				foreach(range($start_position, $end_position) as $current_position){
					if(isset($response['items'][$current_position % ($page * $limit)])){
						$this->items[] = [
							'class' => $this->modelClass?$this->modelClass:NULL,
							'object' => $response['items'][$current_position % $limit],
							'target' => $this->model->target(),
							'lazy_load' => $_lazy_load
						];
					}
				}
			}

			$this->total = intval($response['total']);
		}else{
			$response = $model->execute(
							ORM::METHOD_FOREIGN_LIST, 
							array_merge(['limit' => 0, 'foreign_model' => $foreign_key], ['filter' => $filters])
						);

			if(isset($response['_object'])){
				$class = \Flowlu\Model::_findModelClass($response['_object']);

				foreach($response['items'] as $object){
					$this->items[] = new $class($object['id'], $object);
				}
			}

			$this->total = intval($response['total']);
		}
	}

	private function _load_page($page)
	{
		$page = intval($page);
		$response = $this->model->execute(\Flowlu\ORM::METHOD_LIST, ['page' => $page, 'result_id' => $this->result_id]);
		$limit = intval($response['count']);

		$start_position = ($page - 1) * $limit;
		$end_position = $page * $limit - 1;
		foreach(range($start_position, $end_position) as $current_position){
			if(isset($response['items'][$current_position % $limit])){
				$this->items[] = [
					'class' => $this->modelClass?$this->modelClass:NULL,
					'object' => $response['items'][$current_position % $limit],
					'target' => $this->model->target()
				];
			}
		}

		if($page > 1){
			$start_position = ($page - 2) * $limit;
			$end_position = ($page - 1) * $limit - 2;
			foreach(range($start_position, $end_position) as $current_position){
				if(isset($this->items[$current_position])){
					$this->items[$current_position] = NULL;
				}
			}
		}
	}

	public function rewind() {
		$this->index = 0;
	}

	public function current() {
		if(count($this->items) === 0 && $this->total > 0 || ($this->index + 1) === count($this->items) && ($this->index + 1) < $this->total && count($this->items) !== 1){
			$this->page += 1;
			$this->_load_page($this->page);
		}
		
		if(!isset($this->items[$this->index])){
			return NULL;
		}

		if(is_object($this->items[$this->index])){
			return $this->items[$this->index];
		}else{
			$item = $this->items[$this->index];

			if($item['class']){
				return new $item['class']($item['object']['id'], $item['object'], TRUE);
			}else{
				return new \Flowlu\ORM(array_merge($item['target'], ['id' => $item['object']['id']]), $item['object'], TRUE);
			}
		}
	}

	public function key() {
		return $this->items[$this->index]->id;
	}

	public function next() {
		$this->index += 1;

		if($this->index > count($this->items) && $this->index < $this->total){
			$this->page += 1;
			$this->_load_page($this->page);
		}
	}

	public function valid() {
		// return isset($this->items[$this->index]);
		return $this->index < $this->total;
	}

	public function asArray() {
		// if (count(items) < total) => loadAll
		return $this->items;
	}

	public function count() {
		return $this->total;
	}
}