<?php

namespace Flowlu\Finance;

class Organization extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'organization'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'organization';
}