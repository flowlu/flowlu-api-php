<?php

namespace Flowlu\Finance;

class ActStatus extends \Flowlu\Model
{
    protected $target = [
        'module' => 'finacts',
        'model'  => 'status'
    ];

    protected static $__module = 'finacts';
    protected static $__model = 'status';

    const TYPE_SIGNED = 20;

    const TYPE_NOT_SIGNED = 10;

}