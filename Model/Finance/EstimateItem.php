<?php

namespace Flowlu\Finance;

class EstimateItem extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'estimate_item'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'estimate_item';
}