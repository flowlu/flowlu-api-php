<?php

namespace Flowlu\Finance;

class Invoice extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'invoice'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'invoice';

    const STATUS_NEW = 10;

    const STATUS_NEED_APPROVE = 12;

    const STATUS_APPROVED = 13;

    const STATUS_PAID_PARTIALLY = 20;

    const STATUS_OVERPAID = 30;

    const STATUS_PAID = 40;

}