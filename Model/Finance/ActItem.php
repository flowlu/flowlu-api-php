<?php

namespace Flowlu\Finance;

class ActItem extends \Flowlu\Model
{
    protected $target = [
        'module' => 'finacts',
        'model'  => 'item'
    ];

    protected static $__module = 'finacts';
    protected static $__model = 'item';
}