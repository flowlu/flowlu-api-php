<?php

namespace Flowlu\Finance;

class Estimate extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'estimate'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'estimate';

    const STATUS_DRAFT = 10;

    const STATUS_SEND = 20;

    const STATUS_NOT_ACCEPTED = 30;

    const STATUS_ACCEPTED = 40;

    const STATUS_INVOICED = 50;

    const STATUS_EXPIRED = 60;

}