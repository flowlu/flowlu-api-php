<?php

namespace Flowlu\Finance;

class OrganizationAccount extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'accounts'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'accounts';

    const TYPE_BANK_CASH = 101;

    const TYPE_BANK_BANK = 102;

    const TYPE_BANK_BANK_RUSSIA = 103;

    const TYPE_PS_PAYPAL = 301;

    const TYPE_PS_2CHECKOUT = 302;

    const DEACTIVATED = 0;

    const ACTIVATED = 1;

}