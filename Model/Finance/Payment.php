<?php

namespace Flowlu\Finance;

class Payment extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'payin'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'payin';

    const TYPE_PAYIN = 10;

    const TYPE_PAYIN_PROJECT = 11;

    const TYPE_PAYOUT = 20;

    const TYPE_PAYOUT_PROJECT = 21;

    const TYPE_TRANSFER = 30;

    const TYPE_REFUND = 40;

    const STATUS_NEW = 0;

    const STATUS_COMPLETE = 10;

}