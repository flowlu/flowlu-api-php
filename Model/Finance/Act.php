<?php

namespace Flowlu\Finance;

class Act extends \Flowlu\Model
{
    protected $target = [
        'module' => 'finacts',
        'model'  => 'act'
    ];

    protected static $__module = 'finacts';
    protected static $__model = 'act';
}