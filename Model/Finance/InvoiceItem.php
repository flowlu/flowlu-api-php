<?php

namespace Flowlu\Finance;

class InvoiceItem extends \Flowlu\Model
{
    protected $target = [
        'module' => 'fin',
        'model'  => 'invoice_item'
    ];

    protected static $__module = 'fin';
    protected static $__model = 'invoice_item';
}