<?php

namespace Flowlu\Tasks;

class TimelogEntry extends \Flowlu\Model
{
    protected $target = [
        'module' => 'task',
        'model'  => 'time'
    ];

    protected static $__module = 'task';
    protected static $__model = 'time';
}