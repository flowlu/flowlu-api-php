<?php

namespace Flowlu\Tasks;

class Task extends \Flowlu\Model
{
    protected $target = [
        'module' => 'task',
        'model'  => 'task'
    ];

    protected static $__module = 'task';
    protected static $__model = 'task';

    const TYPE_TASK = 0;

    const TYPE_INBOX_TASK = 1;

    const TYPE_MILESTONE = 10;

    const TYPE_EVENT = 20;

    const TYPE_EVENT_CALLBACK = 21;

    const TYPE_EVENT_EMAIL = 22;

    const TYPE_EVENT_MEET = 23;

    const TYPE_TEMPLATE_TASK = 30;

    const STATE_NEW = 1;

    const STATE_IN_PROGRESS = 3;

    const STATE_SUPPOSEDLY_COMPLETED = 4;

    const STATE_COMPLETED = 5;

    const PRIORITY_LOW = 1;

    const PRIORITY_AVERAGE = 2;

    const PRIORITY_HIGH = 3;

    const RATING_GOOD = 5;

    const RATING_BAD = 1;

    const RATING_NORMAL = 0;

}