<?php

namespace Flowlu\Projects;

class BoardStage extends \Flowlu\Model
{
    protected $target = [
        'module' => 'st',
        'model'  => 'stages'
    ];

    protected static $__module = 'st';
    protected static $__model = 'stages';
}