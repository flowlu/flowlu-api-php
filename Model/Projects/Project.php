<?php

namespace Flowlu\Projects;

class Project extends \Flowlu\Model
{
    protected $target = [
        'module' => 'st',
        'model'  => 'project'
    ];

    protected static $__module = 'st';
    protected static $__model = 'project';
}