<?php

namespace Flowlu\Projects;

class Board extends \Flowlu\Model
{
    protected $target = [
        'module' => 'st',
        'model'  => 'type'
    ];

    protected static $__module = 'st';
    protected static $__model = 'type';
}