<?php

namespace Flowlu\RecordsLists;

class RecordsListField extends \Flowlu\Model
{
    protected $target = [
        'module' => 'customlists',
        'model'  => 'fields'
    ];

    protected static $__module = 'customlists';
    protected static $__model = 'fields';
}