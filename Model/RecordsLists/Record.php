<?php

namespace Flowlu\RecordsLists;

class Record extends \Flowlu\Model
{
    protected $target = [
        'module' => 'customlists',
        'model'  => 'item'
    ];

    protected static $__module = 'customlists';
    protected static $__model = 'item';
}