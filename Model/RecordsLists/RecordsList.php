<?php

namespace Flowlu\RecordsLists;

class RecordsList extends \Flowlu\Model
{
    protected $target = [
        'module' => 'customlists',
        'model'  => 'lists'
    ];

    protected static $__module = 'customlists';
    protected static $__model = 'lists';
}