<?php

namespace Flowlu\Knowledgebase;

class Article extends \Flowlu\Model
{
    protected $target = [
        'module' => 'knowledgebase',
        'model'  => 'articles'
    ];

    protected static $__module = 'knowledgebase';
    protected static $__model = 'articles';
}