<?php

namespace Flowlu\Knowledgebase;

class ArticleVersion extends \Flowlu\Model
{
    protected $target = [
        'module' => 'knowledgebase',
        'model'  => 'versions'
    ];

    protected static $__module = 'knowledgebase';
    protected static $__model = 'versions';
}