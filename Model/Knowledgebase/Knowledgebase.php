<?php

namespace Flowlu\Knowledgebase;

class Knowledgebase extends \Flowlu\Model
{
    protected $target = [
        'module' => 'knowledgebase',
        'model'  => 'knowledgebase'
    ];

    protected static $__module = 'knowledgebase';
    protected static $__model = 'knowledgebase';
}