<?php

namespace Flowlu\Customfields;

class Fieldset extends \Flowlu\Model
{
    protected $target = [
        'module' => 'customfields',
        'model'  => 'fieldsets'
    ];

    protected static $__module = 'customfields';
    protected static $__model = 'fieldsets';
}