<?php

namespace Flowlu\Customfields;

class Field extends \Flowlu\Model
{
    protected $target = [
        'module' => 'customfields',
        'model'  => 'fields'
    ];

    protected static $__module = 'customfields';
    protected static $__model = 'fields';

    const TYPE_SMALLTEXT = 'smalltext';

    const TYPE_TEXT = 'text';

    const TYPE_INT = 'int';

    const TYPE_PRICE = 'price';

    const TYPE_DATE = 'date';

    const TYPE_DATETIME = 'datetime';

    const TYPE_SELECT_SINGLE = 'select.single';

    const TYPE_SELECT_MULTIPLE = 'select.multiple';

    const TYPE_FILE = 'model.file';

    const TYPE_USER = 'model.user';

    const TYPE_BOOLEAN = 'boolean';

    const TYPE_AUTONUMBER = 'autonumber';

}