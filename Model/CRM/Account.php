<?php

namespace Flowlu\CRM;

class Account extends \Flowlu\Model
{
    protected $target = [
        'module' => 'crm',
        'model'  => 'account'
    ];

    protected static $__module = 'crm';
    protected static $__model = 'account';

    const TYPE_COMPANY = 1;

    const TYPE_CONTACT = 2;

}