<?php

namespace Flowlu\CRM;

class Relation extends \Flowlu\Model
{
    protected $target = [
        'module' => 'crm',
        'model'  => 'relation'
    ];

    protected static $__module = 'crm';
    protected static $__model = 'relation';
}