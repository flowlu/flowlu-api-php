<?php

namespace Flowlu\CRM;

class Deal extends \Flowlu\Model
{
    protected $target = [
        'module' => 'crm',
        'model'  => 'lead'
    ];

    protected static $__module = 'crm';
    protected static $__model = 'lead';

    const STATE_ACTIVE = 1;

    const STATE_CLOSED_FAIL = 2;

    const STATE_CLOSED_SUCCESS = 3;

}