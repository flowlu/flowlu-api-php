<?php

namespace Flowlu\CRM;

class Pipeline extends \Flowlu\Model
{
    protected $target = [
        'module' => 'crm',
        'model'  => 'pipeline'
    ];

    protected static $__module = 'crm';
    protected static $__model = 'pipeline';
}