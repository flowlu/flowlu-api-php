<?php

namespace Flowlu\Agile;

class IssueCategory extends \Flowlu\Model
{
    protected $target = [
        'module' => 'agile',
        'model'  => 'category'
    ];

    protected static $__module = 'agile';
    protected static $__model = 'category';
}