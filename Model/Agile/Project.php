<?php

namespace Flowlu\Agile;

class Project extends \Flowlu\Model
{
    protected $target = [
        'module' => 'agile',
        'model'  => 'project'
    ];

    protected static $__module = 'agile';
    protected static $__model = 'project';
}