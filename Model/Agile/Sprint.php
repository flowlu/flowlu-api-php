<?php

namespace Flowlu\Agile;

class Sprint extends \Flowlu\Model
{
    protected $target = [
        'module' => 'agile',
        'model'  => 'sprint'
    ];

    protected static $__module = 'agile';
    protected static $__model = 'sprint';
}