<?php

namespace Flowlu\Agile;

class Workflow extends \Flowlu\Model
{
    protected $target = [
        'module' => 'agile',
        'model'  => 'workflow'
    ];

    protected static $__module = 'agile';
    protected static $__model = 'workflow';
}