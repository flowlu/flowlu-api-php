<?php

namespace Flowlu\Agile;

class WorkflowStage extends \Flowlu\Model
{
    protected $target = [
        'module' => 'agile',
        'model'  => 'stage'
    ];

    protected static $__module = 'agile';
    protected static $__model = 'stage';

    const STATUS_TODO = 10;

    const STATUS_DOING = 20;

    const STATUS_VERIFY = 25;

    const STATUS_DONE = 30;

}