<?php

namespace Flowlu\Agile;

class Issue extends \Flowlu\Model
{
    protected $target = [
        'module' => 'agile',
        'model'  => 'issue'
    ];

    protected static $__module = 'agile';
    protected static $__model = 'issue';

    const PRIORITY_TYPE_BLOCKER = 10;

    const PRIORITY_TYPE_CRITICAL = 20;

    const PRIORITY_TYPE_HIGH = 30;

    const PRIORITY_TYPE_MEDIUM = 40;

    const PRIORITY_TYPE_LOW = 50;

    const PRIORITY_TYPE_LOWEST = 60;

}