<?php

namespace Flowlu\Core;

class User extends \Flowlu\Model
{
    protected $target = [
        'module' => 'core',
        'model'  => 'user'
    ];

    protected static $__module = 'core';
    protected static $__model = 'user';
}