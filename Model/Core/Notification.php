<?php

namespace Flowlu\Core;

class Notification extends \Flowlu\Model
{
    protected $target = [
        'module' => 'core',
        'model'  => 'notification'
    ];

    protected static $__module = 'core';
    protected static $__model = 'notification';
}