<?php

namespace Flowlu\Core;

class ScheduledJob extends \Flowlu\Model
{
    protected $target = [
        'module' => 'core',
        'model'  => 'scheduledjob'
    ];

    protected static $__module = 'core';
    protected static $__model = 'scheduledjob';
}