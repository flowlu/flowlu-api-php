<?php

namespace Flowlu\Core;

class Webhook extends \Flowlu\Model
{
    protected $target = [
        'module' => 'core',
        'model'  => 'webhooksout'
    ];

    protected static $__module = 'core';
    protected static $__model = 'webhooksout';
}