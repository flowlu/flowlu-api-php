<?php

namespace Flowlu\Products;

class Product extends \Flowlu\Model
{
    protected $target = [
        'module' => 'products',
        'model'  => 'product'
    ];

    protected static $__module = 'products';
    protected static $__model = 'product';

    const TYPE_PRODUCT = 1;

    const TYPE_SERVICE = 2;

}