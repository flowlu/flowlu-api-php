<?php

namespace Flowlu\Products;

class Pricelist extends \Flowlu\Model
{
    protected $target = [
        'module' => 'products',
        'model'  => 'pricelist'
    ];

    protected static $__module = 'products';
    protected static $__model = 'pricelist';
}