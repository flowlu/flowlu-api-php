<?php

namespace Flowlu\Products;

class Category extends \Flowlu\Model
{
    protected $target = [
        'module' => 'products',
        'model'  => 'category'
    ];

    protected static $__module = 'products';
    protected static $__model = 'category';
}