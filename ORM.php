<?php

namespace Flowlu;

class ORM
{
	const METHOD_GET 	= 'get';
	const METHOD_LIST 	= 'list';
	const METHOD_CREATE = 'create';
	const METHOD_UPDATE = 'update';
	const METHOD_DELETE = 'delete';

	const METHOD_FOREIGN_LIST = 'foreign_list';
	const METHOD_FOREIGN_GET  = 'foreign_get';

	protected $target = NULL;
	protected $connection = NULL;

	protected $object = [];
	protected $_original_values = [];

	protected $_changed = false;
	protected $_loaded = false;
	protected $_lazy_load = false;

	public function __construct($target = NULL, $object = NULL, $lazy_load = FALSE){
		$this->target = $target;
		
		if(!$lazy_load){
			if(is_null($object)){
				$this->load();
			}else{
				$this->load(false, $object);
			}
		}else{
			$this->object = $object;
			$this->_lazy_load = TRUE;
		}
	}

	public static function factory($target = NULL){
		$model = new ORM($target);

		return $model;
	}

	public function load($force_reload = false, $object = NULL){
		if($object){
			$this->object = $object;
			$this->_original_values = $this->object;
			$this->target('id', $this->object['id']);
			$this->_loaded = true;

			return $this;
		}

		if(is_null($this->target('id'))){
			return;
		}

		if($force_reload || !$this->_loaded){
			try {
				$result = $this->execute(self::METHOD_GET);
			} catch (Exception $e) {
				return;
			}

			if(is_array($result)){
				$this->object = $result;
				$this->_original_values = $this->object;
				$this->target('id', $this->object['id']);
				$this->_loaded = true;
			}
		}

		return $this;
	}

	public function __call($name, $arguments = []){
		if($name === 'findAll'){
			return $this->_findAll(count($arguments)?$arguments[0]:[]);
		}

		$name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $name));

		try {
			$result = $this->execute($name);
		} catch (Exception $e) {
			
		}

		if(isset($result['items'])){
			return json_decode(json_encode($result['items']));
		}

		if(isset($result['result'])){
			return json_decode(json_encode($result['result']));
		}

		return $result;
	}

	public function target($name = NULL, $value = NULL){
		if(is_null($name)){
			return $this->target;
		}

		if(!is_null($name) && !is_null($value)){
			$this->target[$name] = $value;
		}

		return isset($this->target[$name])?$this->target[$name]:NULL;
	}
	
	public function object(){
		return $this->object;
	}

	public function execute($method, array $data = []){
		$module = $this->target('module');
		$model = $this->target('model');
		$id = $this->target('id');

		$params = [];

		if($method === self::METHOD_LIST || $method === self::METHOD_FOREIGN_LIST){
			$params = $data;
			$data = [];
		}

		try {
			if($method === self::METHOD_FOREIGN_LIST){
				$foreign_model = $params['foreign_model'];
				unset($params['foreign_model']);

				$result = \Flowlu\Connection::execute("api/v1/module/{$module}/{$model}/{$id}/{$foreign_model}", $data, $params);
			}else{
				$result = \Flowlu\Connection::execute("api/v1/module/{$module}/{$model}/{$method}/{$id}", $data, $params);
			}
		} catch (Exception $e) {
			return NULL;
		}

		return $result;
	}

	public function save(){
		if($this->target('id')){
			$this->execute(self::METHOD_UPDATE, $this->object());
		}else{
			$result = $this->execute(self::METHOD_CREATE, $this->object());
			if(isset($result['id'])){
				$this->_set('id', $result['id']);
				$this->target('id', $result['id']);
			}
		}

		$this->_changed = false;
	}

	public function delete(){
		$this->execute(self::METHOD_DELETE);
	}

	public function _findAll($filters = []){
		return new ORMResultList($this, $filters);
	}

	public function loaded(){
		return $this->_loaded;
	}

	public function __set($name, $value){
		$this->_set($name, $value);
	}

	public function _set($name, $value){
		/*if($this->target('id') && !$this->loaded()){
			if($this->_lazy_load){
				throw new Exception('not loaded');
			}else{
				$this->load();
				if(!$this->loaded()){
					throw new Exception('not loaded');
				}
			}
		}*/

		if($value instanceof \FLowlu\ORM){
			if(isset($value->object()['id'])){
				$value = $value->object()['id'];
			}else{
				$value = NULL;
			}
		}

		if(isset($this->object[$name])){
			if($this->object[$name] !== $value){
				$this->object[$name] = $value;
				$this->_changed = true;
			}
		}else{
			$this->object[$name] = $value;
			$_changed = true;
		}
	}

	public function __get($name){
		return $this->_get($name);
	}

	public function _get($name){
		/*if($this->target('id') && !$this->loaded()){
			if($this->_lazy_load){
				throw new Exception('not loaded');
			}else{
				$this->load();
				if(!$this->loaded()){
					throw new Exception('not loaded');
				}
			}
		}*/

		if($this->_lazy_load && !$this->loaded()){
			if(isset($this->object[$name])){
				return $this->object[$name];
			}
			
			$this->load();
		}

		if(isset($this->object[$name])){
			if($name === 'id'){
				return strval($this->object[$name]);
			}

			return $this->object[$name];
		}else{
			throw new \Exception('property '.$name.' does not exists');
		}
	}

	public function values($values = []){
		foreach($values as $key => $value){
			$this->_set($key, $value);
		}
	}
}