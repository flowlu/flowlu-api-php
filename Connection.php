<?php
namespace Flowlu;

class Connection
{
	private static $ch = NULL;

	private static $config = [
		'api_key' 	=> '',
		'host' 		=> '',
		'debug'		=> false
	];

	public static function init($config = [])
	{
		self::setConfig(array_merge(self::$config, $config));
	}

	public static function setConfig($values = NULL){
		$args = func_get_args();

		if(count($args) === 1 && is_array($values)){
			self::$config = $values;
		}

		if(count($args) === 2){
			self::$config[(string)$args[1]] = $args[2];
		}
	}

	public static function execute($url, $data = [], $params = []){
		$response = NULL;

		$normalize = function($ar) use(&$normalize) {
			if(is_array($ar)){
				return array_map(function($entry) use (&$normalize) {
					if(is_array($entry)){
						return $normalize($entry);
					}

					return (string) $entry;
				}, $ar);
			}
		};

		$params = $normalize($params);

		if(function_exists('curl_version')){
			if(is_null(self::$ch)){
				self::$ch = curl_init();
			}
			$ch = self::$ch;

			$_url = trim(self::$config['host'], '/').'/'.trim($url, '/?').'?'.http_build_query(array_merge($params, ['api_key' => self::$config['api_key']]));
			curl_setopt($ch, CURLOPT_URL, $_url);

			if(is_array($data) && count($data)){
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
			}

			curl_setopt($ch, CURLOPT_USERAGENT, 'flowlu-api-php-'.FLOWLU_API_PHP_VERSION);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$responseText = curl_exec($ch);
			//curl_close ($ch);

			if(self::$config['debug']){
				echo("Request: ".$_url.PHP_EOL);
				// echo("Response: ".$responseText.PHP_EOL);
			}

			$response = json_decode($responseText, true);
		}

		if(is_array($response) && (isset($response['response']) || isset($response['error']))){
			if(isset($response['response'])){
				return $response['response'];
			}else{
				throw new \Flowlu\ResponseException(
					isset($response['error']['error_code'])? $response['error']['error_code']: 0,
					isset($response['error']['error_msg'])? $response['error']['error_msg']: NULL,
					$_url,
					$responseText
				);
			}
		}else{
			throw new \Flowlu\ResponseException(
				\Flowlu\ResponseException::ERROR_BAD_RESPONSE,
				NULL, 
				$_url,
				$responseText
			);
		}


		if(is_array($response)){
			if(isset($response['response'])){
				return $response['response'];
			}else{
				if(isset($response['error']) && is_array($response['error'])){
					throw new ResponseException(
						isset($response['error']['error_msg'])?
							$response['error']['error_msg']."\r\nRequest: ".$_url :
							'', $response['error']['error_code']);
				}
			}

			return $response;
		}else{
			return [];
		}
	}
}