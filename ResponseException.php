<?php
namespace Flowlu;

class ResponseException extends \Exception
{
	const ERROR_BAD_RESPONSE			= 0;

	const ERROR_UNKNOWN 				= 10;
	const ERROR_AUTHORIZATION_FAILED	= 11;
	const ERROR_INVALID_REQUEST 		= 12;
	const ERROR_INTERNAL_SERVER_ERROR	= 13;

	const ERROR_NOT_FOUND				= 20;
	const ERROR_VALIDATION 				= 21;
	const ERROR_ACCESS_DENIED 			= 22;

	const ERROR_UNKNOWN_METHOD			= 30;
	const ERROR_UNKNOWN_MODULE			= 31;
	const ERROR_UNKNOWN_MODEL 			= 32;

	protected static $default_messages = [
		self::ERROR_UNKNOWN 				=> 'UNKNOWN',
		self::ERROR_AUTHORIZATION_FAILED 	=> 'AUTHORIZATION_FAILED',
		self::ERROR_INVALID_REQUEST 		=> 'INVALID_REQUEST',
		self::ERROR_INTERNAL_SERVER_ERROR 	=> 'INTERNAL_SERVER_ERROR',

		self::ERROR_NOT_FOUND 			=> 'NOT_FOUND',
		self::ERROR_VALIDATION 			=> 'VALIDATION',
		self::ERROR_ACCESS_DENIED 		=> 'ACCESS_DENIED',

		self::ERROR_UNKNOWN_METHOD 		=> 'UNKNOWN_METHOD',
		self::ERROR_UNKNOWN_MODULE 		=> 'UNKNOWN_MODULE',
		self::ERROR_UNKNOWN_MODEL 		=> 'UNKNOWN_MODEL',
	];

	protected $requestUrl = NULL;
	protected $responseText = NULL;

	public function __construct($code, $message = NULL, $requestUrl = NULL, $responseText = NULL)
	{
		$this->code = $code;
		$this->message = $message;

		if(is_null($message) && isset(self::$default_messages[$this->code])){
			$this->message = self::$default_messages[$this->code];
		}else{
			if($this->code === self::ERROR_BAD_RESPONSE){
				$this->message = $responseText; 
			}
		}

		$this->requestUrl = $requestUrl;
		$this->responseText = $responseText;
	}

	public function getRequestUrl()
	{
		return $this->requestUrl;
	}

	public function getResponseText()
	{
		return $this->responseText;
	}
}