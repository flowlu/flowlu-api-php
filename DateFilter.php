<?php
namespace Flowlu;

class DateFilter
{
	protected $startDate = NULL;
	protected $endDate = NULL;

	public function __construct()
	{
		$args = func_get_args();

		if(count($args) === 1){
			if(is_array($args[0])){
				if(isset($args[0]['startDate'])){
					$this->startDate = $args[0]['startDate'];
				}

				if(isset($args[0]['endDate'])){
					$this->endDate = $args[0]['endDate'];
				}
			}else{
				$this->startDate = $args[0];
			}
		}

		if(count($args) === 2){
			$this->startDate = $args[0];
			$this->endDate = $args[1];
		}

		foreach(['startDate', 'endDate'] as $field){
			if(!is_null($this->{$field})){
				if(is_object($this->{$field}) && $this->{$field} instanceof \DateTime){
					$this->{$field} = $this->{$field}->format('Y-m-d');
				}elseif(is_string($this->{$field})){
					$this->{$field} = (new \DateTime($this->{$field}))->format('Y-m-d');
				}else{
					$this->{$field} = NULL;
				}
			}
		}
	}

	public function __toString()
	{
		$_result = [];
		foreach(['startDate', 'endDate'] as $field){
			if(!is_null($this->{$field})){
				$_result[strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $field))] = $this->{$field};
			}
		}

		if(count($_result)){
			return json_encode($_result);
		}

		return '';
	}
}