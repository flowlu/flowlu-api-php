<?php

namespace Flowlu;

class Model extends \Flowlu\ORM
{
	protected $target = [
		'module' => NULL,
		'model' => NULL
	];

	protected static $__module = NULL;
	protected static $__model = NULL;

	public function __construct($id = NULL, $object = NULL, $lazy_load = FALSE){
		if(is_null($id)){
			parent::__construct([
				'module' => static::$__module,
				'model' => static::$__model
			], $object, $lazy_load);
		}else{
			parent::__construct([
				'module' => static::$__module,
				'model' => static::$__model,
				'id' => $id
			], $object, $lazy_load);
		}
	}

	public static function find($id){
		if(is_array($id)){
			$_result = self::findAll(array_merge($id, ['limit' => 1]));
			if($current = $_result->current()){
				return $current;
			}else{
				throw new \Flowlu\ModelExceptionNotFound();
			}
		}

		try {
			if(is_int($id)){
				return new static($id);
			}

			if(is_string($id)){
				if(is_numeric($id)){
					return new static($id);
				}else{
					if(get_called_class() === 'Flowlu\Core\User'){
						$_result = self::findAll(['username' => $id]);

						$_result_user = NULL;

						foreach($_result as $_item){
							return $_item;
						}

						throw new \Flowlu\ModelExceptionNotFound();
					}
				}
			}


			if(is_int($id) || is_string($id)){
				return new static($id);
			}
		} catch (\Flowlu\ResponseException $e) {
			if($e->getCode() === \Flowlu\ResponseException::ERROR_NOT_FOUND){
				throw new \Flowlu\ModelExceptionNotFound();
			}else{
				throw $e;
			}
		}
	}

	public static function findOrNull($id){
		try {
			return static::find($id);
		} catch (\Flowlu\ModelExceptionNotFound $e) {
			return NULL;
		}
	}

	public static function findOrException($id){
		return static::find($id);
	}

	public function id(){
		if(isset($this->object['id'])){
			return $this->_get('id');
		}
	}

	public static function findAll($filters = []){
		return ORM::factory([
			'module' => static::$__module,
			'model' => static::$__model
		])->findAll($filters);
	}

	public function get($field){

	}

	public function getAll($field, $filters = []){
		return new \Flowlu\ORMResultList($this, $filters, $field);
	}

	public function add($field, $model){
		$this->execute(\Flowlu\ORM::METHOD_FOREIGN_CREATE, [
			'foreign_model' => $field
		], $model->object());
	}

	public static function __getModule(){
		return static::$__module;
	}

	public static function __getModel(){
		return static::$__model;
	}

	public static function _findModelClass($target = []){
		$classes = array_filter(get_declared_classes(), function($name){
			if(strpos($name, 'Flowlu\\') === 0){
				return true;
			}

			return false;
		});

		foreach($classes as $class){
			if(in_array($class, [
				'Flowlu\ORM', 
				'Flowlu\Request', 
				'Flowlu\Model', 
				'Flowlu\ORMResultList', 
				'Flowlu\Connection',
				'Flowlu\ResponseException',
				'Flowlu\ModelException',
				'Flowlu\ModelExceptionNotFound'
			])) continue;


			$obj = new $class();
			if(
				$obj instanceof \Flowlu\Model 
				&& $target['module'] === $obj::__getModule()
				&& $target['model'] === $obj::__getModel()
			){
				return $class;
			}
		}
	}
}