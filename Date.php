<?php
namespace Flowlu;

class Date
{
	public static function humanizeSeconds($seconds = 0){
		$total_seconds = (int) $seconds;

		$hours = floor((int)$total_seconds/60/60);
		$hours = (int)$hours < 10 ? '0' . $hours : $hours;
		$minutes = floor(((int)$total_seconds % 3600)/60);
		$minutes = (int)$minutes < 10 ? '0' . $minutes : $minutes;

		return "{$hours}h {$minutes}m";
	}
}