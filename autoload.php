<?php

require_once('Connection.php');
require_once('DateFilter.php');
require_once('Date.php');
require_once('ORM.php');
require_once('ORMResultList.php');
require_once('Model.php');
require_once('ResponseException.php');

require_once('ModelException.php');
require_once('ModelExceptionNotFound.php');

define("FLOWLU_API_PHP_VERSION", "0.1");

foreach(scandir(implode(DIRECTORY_SEPARATOR, [__DIR__, 'Model'])) as $dir){
	if(in_array($dir, ['.', '..'])) continue;

	if(is_dir(implode(DIRECTORY_SEPARATOR, [__DIR__, 'Model', $dir]))){
		foreach(scandir(implode(DIRECTORY_SEPARATOR, [__DIR__, 'Model', $dir])) as $file){
			if(is_file(implode(DIRECTORY_SEPARATOR, [__DIR__, 'Model', $dir, $file]))){
				require_once(implode(DIRECTORY_SEPARATOR, [__DIR__, 'Model', $dir, $file]));
			}
		}
	}
}